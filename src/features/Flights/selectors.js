import { createSelector } from "reselect";
import { ALL_CARRIERS } from "./dictionary";

export const flightsReducerSelector = state => state.flights;

export const flightsSelector = state => flightsReducerSelector(state).flights;

export const carrierSelector = state => flightsReducerSelector(state).carrier;

export const carriersSelector = state => flightsReducerSelector(state).carriers;

export const isLoadingSelector = state =>
  flightsReducerSelector(state).isLoading;

export const carrierFlightsSelector = createSelector(
  [flightsSelector, carrierSelector],
  (flights, carrier) => {
    if (carrier === ALL_CARRIERS) return flights;

    return flights.filter(flight => flight.carrier === carrier);
  }
);
