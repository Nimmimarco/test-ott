import { all, put, take, call, fork } from "redux-saga/effects";
import { delay } from "redux-saga";
import keyBy from "lodash/keyBy";

import data from "./data";

import {
  REQUEST_FLIGHTS,
  REQUEST_FLIGHTS_SUCCESS,
  REQUEST_FLIGHTS_FAILURE
} from "./";

function* getFlights() {
  try {
    yield call(delay, 250);

    const flights =
      (!!data && !!data.flights && !!data.flights.length && data.flights) || [];

    yield put({
      type: REQUEST_FLIGHTS_SUCCESS,
      payload: {
        flights,
        carriers: Object.keys(keyBy(flights, "carrier") || {})
      }
    });
  } catch (err) {
    yield put({
      type: REQUEST_FLIGHTS_FAILURE,
      err
    });
  }
}

export function* watchFlights() {
  while (true) {
    yield take(REQUEST_FLIGHTS);
    yield fork(getFlights);
  }
}

export default function* watchFlightsSaga() {
  yield all([fork(watchFlights)]);
}
