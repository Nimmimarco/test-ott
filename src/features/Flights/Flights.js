import React, { PureComponent, Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

import { formateDate } from "./utils";
import { ALL_CARRIERS } from "./dictionary";

const selectStyles = theme => ({
  container: {
    width: "100%",
    marginBottom: 16
  }
});

const flightStyles = theme => ({
  container: {
    padding: 8
  },
  title: {
    display: "block",
    margin: "0 0 16px 0"
  },
  fromTo: {
    display: "block",
    marginBottom: 8
  },
  departure: {
    display: "block"
  },
  arrival: {
    extend: "arrival"
  }
});

class FlightsSelectComp extends PureComponent {
  handleChange = event => {
    this.props.selectCarrier(event.target.value);
  };

  render() {
    const { classes, carriers } = this.props;
    return (
      <FormControl className={classes.container}>
        <InputLabel htmlFor="carrier-native">Авиакомпания</InputLabel>
        <Select
          native
          onChange={this.handleChange}
          inputProps={{
            name: "carrier",
            id: "carrier-native"
          }}
        >
          <option>{ALL_CARRIERS}</option>
          {!!carriers &&
            !!carriers.length &&
            carriers.map((carrier, idx) => (
              <option key={`${carrier}-${idx}`}>{carrier}</option>
            ))}
        </Select>
      </FormControl>
    );
  }
}

const FlightsSelect = withStyles(selectStyles)(FlightsSelectComp);

const FlightComp = ({
  classes,
  flight: { carrier, arrival, departure, direction }
}) => (
  <div className={classes.container}>
    <h3 className={classes.title}>{carrier}</h3>
    <span className={classes.fromTo}>
      {direction.from} -> {direction.to}
    </span>
    <span className={classes.departure}>
      Время отправления:<br />
      {formateDate(departure)}
    </span>
    <span className={classes.arrival}>
      Время прибытия:<br />
      {formateDate(arrival)}
    </span>
  </div>
);
const Flight = withStyles(flightStyles)(FlightComp);

const FlightsGrid = ({ flights }) => (
  <Grid container justify="center" spacing={16}>
    {flights.map(flight => (
      <Grid key={flight.id} item sm={12} md={6}>
        <Paper elevation={4}>
          <Flight flight={flight} />
        </Paper>
      </Grid>
    ))}
  </Grid>
);

class Flights extends PureComponent {
  componentDidMount() {
    this.props.requestFlights();
  }

  render() {
    const { flights, carriers, selectCarrier, isLoading } = this.props;

    if (isLoading) return <Fragment>Загрузка...</Fragment>;

    if (!flights.length) return <Fragment>Нет данных</Fragment>;

    return (
      <Fragment>
        <FlightsSelect carriers={carriers} selectCarrier={selectCarrier} />
        <FlightsGrid flights={flights} />
      </Fragment>
    );
  }
}

export default Flights;
