import { ALL_CARRIERS } from "./dictionary";

export const REQUEST_FLIGHTS = "features/flights/REQUEST_FLIGHTS";
export const REQUEST_FLIGHTS_SUCCESS =
  "features/flights/REQUEST_FLIGHTS_SUCCESS";
export const REQUEST_FLIGHTS_FAILURE =
  "features/flights/REQUEST_FLIGHTS_FAILURE";

export const SELECT_CARRIER = "features/flights/SELECT_CARRIER";

const initialState = {
  flights: [],
  carriers: [],
  carrier: ALL_CARRIERS,
  isLoading: false
};

export default function flightsReducer(state = initialState, action = {}) {
  switch (action.type) {
    case REQUEST_FLIGHTS:
      return {
        ...state,
        isLoading: true
      };
    case REQUEST_FLIGHTS_SUCCESS:
      return {
        ...state,
        flights: action.payload.flights,
        carriers: action.payload.carriers,
        isLoading: false
      };
    case SELECT_CARRIER:
      return {
        ...state,
        carrier: action.payload.carrier
      };
    default:
      return state;
  }
}

export const requestFlights = () => ({
  type: REQUEST_FLIGHTS
});

export const selectCarrier = carrier => ({
  type: SELECT_CARRIER,
  payload: { carrier }
});
