import { connect } from "react-redux";

import Flights from "./Flights";
import {
  requestFlights,
  selectCarrier,
  carrierFlightsSelector,
  carriersSelector,
  isLoadingSelector
} from "./";

const mapStateToProps = state => ({
  flights: carrierFlightsSelector(state),
  carriers: carriersSelector(state),
  isLoading: isLoadingSelector(state)
});
const mapDispatchToProps = dispatch => ({
  requestFlights: () => dispatch(requestFlights()),
  selectCarrier: carrier => dispatch(selectCarrier(carrier))
});

const FlightsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Flights);

export default FlightsContainer;
