import { format } from "date-fns";

export const formateDate = date => format(date, "hh:mm A, DD-MM-YYYY");
