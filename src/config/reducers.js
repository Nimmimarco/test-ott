import { combineReducers } from "redux";

import { flightsReducer } from "./../features/Flights";

export default combineReducers({
  flights: flightsReducer
});
