import createSagaMiddleware from "redux-saga";
import { createLogger } from "redux-logger";

const middlewares = [];

const sagaMiddleware = createSagaMiddleware();
middlewares.push(sagaMiddleware);

if (process.env.NODE_ENV !== "production") {
  const logger = createLogger({
    collapsed: false
  });
  middlewares.push(logger);
}

export { middlewares as default, sagaMiddleware };
