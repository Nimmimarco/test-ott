import { all, fork } from "redux-saga/effects";

import { watchFlightsSaga } from "./../features/Flights";

export default function* rootSaga() {
  yield all([fork(watchFlightsSaga)]);
}
