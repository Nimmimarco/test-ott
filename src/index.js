import configureStore from "./config/store";
import { AppContainer, renderApp } from "./App";
import registerServiceWorker from "./registerServiceWorker";

const store = configureStore({});
const rootElement = document.getElementById("root");

renderApp(AppContainer, store, rootElement);
registerServiceWorker();
